# Copyright (c) OpenMMLab. All rights reserved.
import copy
import os.path as osp
from typing import List, Union

from mmengine.fileio import get_local_path

from mmdet.registry import DATASETS
from .base_det_dataset import BaseDetDataset
import cv2
from mmengine.utils import ProgressBar
from concurrent.futures import ProcessPoolExecutor


@DATASETS.register_module()
class YoloDataset(BaseDetDataset):
    """Dataset for COCO."""

    METAINFO = {
        'classes':
        ('handbag', 'backpack', 'suitcase', 'knife', 'gun'),
        # palette is a list of color tuples, which is used for visualization.
        'palette':
        [(220, 20, 60), (119, 11, 32), (0, 0, 142), (0, 0, 230), (106, 0, 228)]
    }

    def __init__(self, data_root, ann_file, **kwargs):
        path = osp.join(data_root, ann_file)
        with open(path, 'r') as f:
            self.image_ls = [l for l in f.read().split("\n") if l != ""]
        
        super().__init__(data_root=data_root, ann_file=ann_file, **kwargs)
    
    def load_image(self, param):
        img_path, i = param
        label_path = osp.splitext(img_path.replace(
            "/images", "/labels"))[0] + ".txt"
        h, w = cv2.imread(img_path).shape[:-1]
        with open(label_path, 'r') as f:
            lines = [l for l in f.read().split("\n") if l != '']

        instances = []
        for line in lines:
            line = line.split(" ")
            idx = int(line[0])

            x1, y1, x2, y2 = list(map(float, line[1:]))
            x1, y1 = x1 - 0.5 * x2, y1 - 0.5 * y2
            x2, y2 = x2 + x1, y2 + y1
            x1, x2 = x1 * w, x2 * w
            y1, y2 = y1 * h, y2 * h
            bbox = list(map(int, [x1, y1, x2, y1]))

            instances.append(dict(bbox=bbox,
                                    bbox_label=idx))
        
        return instances, img_path, w, h, i

    def load_data_list(self) -> List[dict]:
        data_list = []
        prog_bar = ProgressBar(len(self.image_ls))
        
        for i, img_path in enumerate(self.image_ls):
            label_path = osp.splitext(img_path.replace(
                "/images", "/labels"))[0] + ".txt"
            h, w = cv2.imread(img_path).shape[:-1]
            with open(label_path, 'r') as f:
                lines = [l for l in f.read().split("\n") if l != '']

            instances = []
            for line in lines:
                line = line.split(" ")
                idx = int(line[0])

                x1, y1, x2, y2 = list(map(float, line[1:]))
                x1, y1 = x1 - 0.5 * x2, y1 - 0.5 * y2
                x2, y2 = x2 + x1, y2 + y1
                x1, x2 = x1 * w, x2 * w
                y1, y2 = y1 * h, y2 * h
                bbox = list(map(int, [x1, y1, x2, y1]))

                instances.append(dict(bbox=bbox,
                                      bbox_label=idx, 
                                      ignore_flag=0))
        
            data_list.append(dict(img_path=img_path,
                                img_id=i,
                                width=w,
                                height=h,
                                instances=instances))
            
            prog_bar.update()
        
        # with ProcessPoolExecutor(max_workers=4) as pool:
        #     indexes = list(range(len(self.image_ls)))
        #     for instances, img_path, w, h, i in pool.map(self.load_image, zip(self.image_ls, indexes)):
        #         data_list.append(dict(img_path=img_path,
        #                             img_id=i,
        #                             width=w,
        #                             height=h,
        #                             instances=instances))
        #         prog_bar.update()

        return data_list
