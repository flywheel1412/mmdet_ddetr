#! /bin/bash

export OMP_NUM_THREADS=8
export MKL_NUM_THREADS=8

./tools/dist_train.sh \
    configs/deformable_detr/deformable-detr_r50_16xb2-50e_coco.py \
    4 